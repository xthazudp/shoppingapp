const http = require('http');
const fileOp = require('./write');

// http is hypertext transfer protocol
// http server 
// http client
var server = http.createServer(function (request, response) {
    console.log('client connted to server')
    console.log('request url', request.url)
    console.log('request method', request.method)
    var a = req.url.split('/');
    var action = a[1];
    var fileName = a[2];
    var content = a[3];
    // request or 1st arugment is always http request object
    // response or 2nd argument is always http response object

    // response must be sent
    // request response cycle must be completed
    // response.end('Welcome to Nodejs Server');

    // callback for handling http client request
    // http verb
    // GET, POST,DELETE,PUT,
    // http status
    // 100,200,300,400,500

    // endpoint ==> combination of http method and url

    // regardless of any http method and url this callback function will be executed
    // this callback is handler for every http request

    if (request.url === '/write') {
        fileOp.a('broadway.txt', 'welcome to broadway infosys nepal', function (err, done) {
            if (err) {
                response.end('firl writing failed >>' + err);
            } else {
                response.end('file writing success' + done);
            }
        })
    }
    else if (request.url === '/read') {
        fileOp.b('broadway.txt')
            .then(function (data) {
                response.end('file reading success >' + data);
            })
            .catch(function (err) {
                response.end('file reading fialed >>' + err)
            })
    }
    else {
        response.end('No any actions to perform')
    }


})

server.listen(9090, function (err, done) {
    if (err) {
        console.log('error in listening >>', err);
    } else {
        console.log('server listening at port 9090')
        console.log('press CTRL + C to exit')
    }
})


