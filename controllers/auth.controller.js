const express = require('express');
const UserModel = require('./../models/user.model');
const router = express.Router(); // routing level middleware
const MAP_USER_REQ = require('./../helpers/map_user_request');
const Uploader = require('./../middlewares/uploader');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');
const randomStr = require('randomstring');

const sender = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'xthazudp3@gmail.com',
    pass: 'account@google1940s',
  },
});

function prepareEmail(data) {
  return {
    from: 'shoppingApp Web Store', // sender address
    to: 'xthazudp3@gmail.com,' + data.email, //comma seperated list of receivers
    subject: 'Forgot Password ✔', // Subject line
    text: 'Forgot Password?', // plain text body
    html: `<p>Hi <strong>${data.name}</strong>,</p>
      <p>We noticied that you are having trouble logging into our system. please click the link below to reset your password</p>
      <p><a href="${data.link}">click here to reset password</a></p>
      <p>The link will be expired within 24 hours and password cane reset will the link only once so please use it wisely</p>
      <p>Regards,</p>
      <p>Group 34 Support Team</p>`, // html body
  };
}

// js Docs commeting style

/**
 * create token with given data
 * @param {object} data
 * @returns string
 */
function createToken(data) {
  let token = jwt.sign(
    {
      _id: data._id,
    },
    config.JWT_SECRECT
  );
  return token;
}

router.get('/', function (req, res, next) {
  require('fs').readFile('abcd', function (err, done) {
    if (err) {
      req.myEvent.emit('error', err, res);
    }
  });
});

router.post('/login', function (req, res, next) {
  UserModel.findOne({
    $or: [{ username: req.body.username }, { email: req.body.username }],
  })
    .then(function (user) {
      if (!user) {
        return next({
          msg: 'Invalid username',
          status: 400,
        });
      }
      if (user.status !== 'active') {
        return next({
          msg: 'Your Account is disabled please contact system administrator for support',
          status: 401,
        });
      }
      // if user
      var isMatched = passwordHash.verify(req.body.password, user.password);
      if (isMatched) {
        // token generation
        var token = createToken(user);
        res.json({
          user: user,
          token: token,
        });
      } else {
        next({
          msg: 'Invalid Password',
          status: 400,
        });
      }
      // token generation
      // complete req-res cycle
    })
    .catch(function (err) {
      next(err);
    });
});

router.post('/register', Uploader.single('image'), function (req, res, next) {
  // console.log('req.bdy. o>>>>', req.body);
  // console.log('req file >>', req.file)
  if (req.fileTypeError) {
    return next({
      msg: 'Invalid File Format',
      status: 400,
    });
  }
  if (req.file) {
    req.body.image = req.file.filename;
  }
  // if there is value in req. file our file is uploaded in server
  // data validation
  // password hashing
  // db operation
  // response

  var newUser = new UserModel({});
  //new user  is mongoose object
  // _id, --v,  default model , timestamps plugin
  // methods are function available to execute db query
  var newMappedUser = MAP_USER_REQ(newUser, req.body);
  newMappedUser.password = passwordHash.generate(req.body.password);

  newMappedUser.save(function (err, done) {
    if (err) {
      return next(err);
    }
    res.json(done);
  });
});

router.post('/forgot-password', function (req, res, next) {
  var email = req.body.email;
  UserModel.findOne({
    email: email,
  })
    .then(function (user) {
      if (!user) {
        return next({
          msg: 'No any account linked with provided email!',
          status: 404,
        });
      }
      // now proceed with email sending

      const resetToken = randomStr.generate(29);

      const emailData = {
        name: user.username,
        email: user.email,
        link: `${req.headers.origin}/reset_password/${resetToken}`,
      };

      const expiryTime = Date.now() + 1000 * 60 * 60 * 24;
      const emailBody = prepareEmail(emailData);
      user.passwordResetExpiryTime = expiryTime;
      user.passwordResetToken = resetToken;

      user.save(function (err, saved) {
        if (err) {
          return next(err);
        }
        sender.sendMail(emailBody, function (err, done) {
          if (err) {
            return next(err);
          }
          res.json(done);
        });
      });
    })
    .catch(function (err) {
      next(err);
    });
});

router.post('/reset-password/:token', function (req, res, next) {
  var token = req.params.token;
  UserModel.findOne(
    {
      passwordResetToken: token,
      passwordResetExpiryTime: {
        $gte: Date.now(),
      },
    },
    function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next({
          msg: 'Password Reset Token Invalid or Expired',
          status: 400,
        });
      }
      if (new Date(user.passwordResetExpiryTime).getTime() <= Date.now()) {
        return next({
          msg: 'Password Reset Token Expired',
          status: 400,
        });
      }
      user.password = passwordHash.generate(req.body.password);
      user.passwordResetToken = null;
      user.passwordResetExpiryTime = null;
      user.save(function (err, saved) {
        if (err) {
          return next(err);
        }
        res.json(saved);
      });
    }
  );
});

module.exports = router;
