//database ==> database is a container/bucket where data are kept

// CRUD==> database operation
// Create Read Update Delete

// Database Management System (DBMS)
// DBMS is used to perform database operation

// 1. Relational DBMS
// 2. Distributed DBMS (non relational)

// Relational DBMS
// 1.Table Based Design ==>
// Entities ==> LMS ==> Books, Users, Notifications, Reviews
// 2.each record are called tuple/row
// each record must validate db schema
// 3. Schema Based Solution
// predefine properties
// 4. Non Scalable ==> we cannot add more properties other then defined in schema
// 5. relation between table exists
// 6. SQL database ==> Structured Query Language
// 7. eg MySQL, sql-lite, postgres,


// Non Relational DBMS
// 1. Collection based approach 
// Entities ===> LMS ==> Books,Users, reviews
// 2. document are recoed inside collection
// document can be any valid JSON object
// 3.Schema less design
// 4.Highly Scalable ==> 
// 5.Relation doesnot exists between collections
// 6. NO SQL ==> NOT ONLY SQL
// 7. eg mongodb,couchdb, dynamodb,redis



// ===> mongodb 
// mongodb is Distributed DBMS

// clinet server architecture
// mongodb server ==> install and run ===> 27017 

// command
// mongod ===> mongod driver intilization

// mongo ===> connects mongo shell with db server

// once we have > interface we are ready to execute shell command
// mongo shell
// db operation CRUD

// show dbs ==> it list all the available database
// use <db_name> if(existing db) select the existing db else create new db and select it

// db ==> prints selected db

// show collections ==> it will list all the available collections from selected database

// to add collection
// CRUD
// C create
// db.<collection_name>.insert({validJSON}) 
// db.<collection_name>.insertMany({validJSON})

// Read
// db.<collection_name>.find({query_builder})
// db.<collection_name>.find({query_builder}).pretty() // format the output
// db.<collection_name>.find({query_builder}).count() // returns number of documents from selected collections
// .sort(order) //sort documents
// .limit(count) // limits the document fetch
// .skip (count) // skip the provided number of document
// query_builder==> object that will be used as a query
// range
// $lt ===> less than $lte less than equals
// $gt ===> greater than $gte grater than equals
// db.bikes.find({
//     price: {
//         $gte: 3444
//     }
// })
// array ==> $in , $all
// eg
// db.bikes.find({colors:{$in:['a','b']}}) either a or b can be matched
// db.bikes.find({colors:{$all:['a','b']}}) //a and b both must be matched
// projection
// find({query_Builder},{project_object})

// Update
// db.<collection_name>.update({},{},{});
// 1st object is for query builder
// 2nd object is  object having $set as key and another object as value
// 3rd object is options and is optional
// eg  db.bikes.update({color:'red'},{$set:{status:'booked'}},{multi:true,upsert:true})

// Delete
// db.<collection_name>.remove({query_builder});
// note do not left the query builder of remove empty

// drop collection
// db.<collection_name>.drop()
// drop database
// db.dropDatabase();


// ODM ===> Object Document Modelling
// mongoose is ODM for mongodb database and nodejs as programming language
// advantages of using ODM

// 1. Schema based Solution
// entities and their attributes are predefined
// 2. data types for value
// 3. indexing are easier
// 4. middleware
// 5. methods for handling db operation

