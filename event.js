const events = require('events');
const myEvent = new events.EventEmitter();

// listener
myEvent.on('shoppingApp', function (data) {
  console.log('at shopping event..', data);
});

// trigger
setTimeout(function () {
  myEvent.emit('shoppingApp', 'from trigger event');
}, 3000);
