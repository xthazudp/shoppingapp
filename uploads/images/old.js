// import { Grid } from '@material-ui/core'
// import { orange, purple } from '@material-ui/core/colors'
// import { createMuiTheme, withStyles } from '@material-ui/core/styles'
// import Table from '@material-ui/core/Table'
// import TablePagination from '@material-ui/core/TablePagination'
// import TableBody from '@material-ui/core/TableBody'
// import TableCell from '@material-ui/core/TableCell'
// import TableRow from '@material-ui/core/TableRow'
// import TableHead from '@material-ui/core/TableHead'

// import _ from 'lodash'
// import PropTypes from 'prop-types'
// import React, { Component } from 'react'
// import { connect } from 'react-redux'
// import { withRouter } from 'react-router-dom'
// import { bindActionCreators } from 'redux'

// import NotLoggedIn from './../../NotLoggedIn'

// const theme = createMuiTheme({
//     palette: {
//         primary: purple,
//         secondary: orange
//     },
//     typography: {
//         useNextVariants: true,
//         fontSize: 12
//     }
// })
// const styles = theme => ({
//     root: {
//         width: '100%',
//         overflowY: 'auto',
//         overflowX: 'auto'
//     },
//     filterRoot: {
//         paddingLeft: 16
//     },
//     content: {
//         marginTop: 12,
//         flexGrow: 1,
//         padding: theme.spacing.unit * 2,
//         height: '82vh',
//         backgroundColor: '#fbf8f896',
//         overflowY: 'hidden'
//     },
//     details: {
//         fontSize: '.92em',
//         margin: '2px 8px 2px 0',
//         padding: '2px 8px 2px 0'
//     },
//     filterRoot: {
//         paddingLeft: 16
//     },
//     title: {
//         flexGrow: 1
//     },
//     scrollArea: {
//         overflowY: 'auto',
//         '-webkitOverflowScrolling': 'touch',
//         height: '82vh'
//     },
//     label: {
//         color: '#9c27b0',
//         padding: '4px 8px 4px 0',
//         fontSize: 11,
//         fontWeight: 600,
//         textTransform: 'uppercase'
//     },
//     pageStatement: {
//         paddingTop: 0,
//         fontSize: 12
//     },
//     spacer: {
//         height: 40
//     },
// })

// const extractDataFromJson = (rawData = {}) => {
//     return Object.values(rawData).reduce((acc, item) => {
//         acc.entities = [
//             ...acc.entities,
//             ...item.json.Entities
//         ];
//         acc.textract_source_blocks = [
//             ...acc.textract_source_blocks,
//             ...item.json.textract_source.Blocks
//         ]
//         return acc;
//     }, { entities: [], textract_source_blocks: [] })
// }

// class EntitiesAnalysisComponent extends Component {
//     constructor() {
//         super()
//         this.state = {
//             page: 1,
//             rowsPerPage: 10,
//             entities: [],
//             textract_source_blocks: []
//         };

//     }

//     componentDidMount() {
//         console.log('raw json >>', this.props.json);
//         console.log('data >>', extractDataFromJson(this.props.json))
//         this.setState({
//             ...extractDataFromJson(this.props.json)
//         });
//     }

//     handleChangePage = (event, newPage) => {
//         this.setState({
//             page: newPage
//         })
//     }

//     handleChangeRowsPerPage = (event) => {
//         this.setState({
//             rowsPerPage: +event.target.value,
//             page: 0
//         })
//     }

//     render() {
//         const { classes, me } = this.props

//         if (!me.user) {
//             return <NotLoggedIn />
//         }


//         return (
//             <Grid container>
//                 <Grid container spacing={0} item>
//                     <main className={classes.content}>
//                         <div className={classes.scrollArea}>
//                             <div className={classes.root}>
//                                 <Table padding="dense">
//                                     <TableHead>
//                                         <TableRow>
//                                             <TableCell width="5%">
//                                                 Entity
//                                             </TableCell>
//                                             <TableCell width="20%">
//                                                 Type
//                                             </TableCell>
//                                             <TableCell
//                                                 width="5%"
//                                             >
//                                                 Category
//                                             </TableCell>
//                                             <TableCell
//                                                 width="10%"
//                                             >
//                                                 Traits
//                                             </TableCell>
//                                         </TableRow>
//                                     </TableHead>
//                                     <TableBody>
//                                         {this.state.entities
//                                             .slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
//                                             .map((item, index) => (
//                                                 <TableRow key={index}>
//                                                     <TableCell variant="body">
//                                                         <div style={{ flex: 1 }}>
//                                                             {item.Text}
//                                                         </div>
//                                                         <div style={{ flex: 1 }}>
//                                                             {item.Score} Score
//                                                     </div>
//                                                     </TableCell>
//                                                     <TableCell variant="body">
//                                                         <div style={{ flex: 1 }}>
//                                                             {item.Type}
//                                                         </div>
//                                                     </TableCell>
//                                                     <TableCell variant="body">
//                                                         <div style={{ flex: 1 }}>
//                                                             {item.Category}
//                                                         </div>
//                                                     </TableCell>
//                                                     <TableCell variant="body">
//                                                         {item.Traits.length
//                                                             ?
//                                                             <>
//                                                                 <div style={{ flex: 1 }}>
//                                                                     {item.Traits[0].Name}
//                                                                 </div>
//                                                                 <div style={{ flex: 1 }}>
//                                                                     {item.Traits[0].Score} Score
//                                                             </div>
//                                                             </>
//                                                             : '-'
//                                                         }

//                                                     </TableCell>
//                                                 </TableRow>
//                                             ))}
//                                     </TableBody>
//                                 </Table>
//                                 <TablePagination
//                                     rowsPerPageOptions={[10, 20, 30]}
//                                     component="div"
//                                     count={this.state.entities.length}
//                                     rowsPerPage={this.state.rowsPerPage}
//                                     page={this.state.page}
//                                     onChangePage={this.handleChangePage}
//                                     onChangeRowsPerPage={this.handleChangeRowsPerPage}
//                                 />
//                             </div>
//                         </div>
//                     </main>
//                 </Grid>
//             </Grid >
//         )
//     }
// }

// EntitiesAnalysisComponent.propTypes = {
//     classes: PropTypes.object.isRequired
// }

// const mapStateToProps = stateFromStore => ({
//     me: stateFromStore.me,
//     lookups: stateFromStore.lookups,
// })

// const mapDispatchToProps = dispatch =>
//     bindActionCreators(
//         {},
//         dispatch
//     )

// export const EntitiesAnalysis = withRouter(
//     connect(
//         mapStateToProps,
//         mapDispatchToProps
//     )(withStyles(styles)(EntitiesAnalysisComponent))
// )

