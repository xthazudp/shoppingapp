const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    // db_modelling
    name: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
    },
    username: {
      type: String,
      lowercase: true,
      trim: true,
      requred: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      minlength: 8,
    },
    dob: {
      type: Date,
    },
    phoneNumber: {
      type: Number,
    },
    address: {
      tempAddress: [String],
      permanentAddress: String,
    },
    country: {
      type: String,
      default: 'nepal',
    },
    gender: {
      type: String,
      enum: ['male', 'female', 'others'],
    },
    status: {
      type: String,
      enum: ['active', 'inActive', 'NotConfirmed'],
      default: 'active',
    },
    role: {
      type: Number, // 1 for admin, 2 for normal user
      default: 2,
    },
    history: {
      type: Array,
      default: [],
    },
    passwordResetExpiryTime: Date,
    passwordResetToken: String,
    image: String,
  },

  {
    timestamps: true,
  }
);
// timestamps will be automated

module.exports = mongoose.model('user', UserSchema);
