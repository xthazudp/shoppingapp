// socket file
const socket = require('socket.io');
const config = require('./configs');

var users = [];
module.exports = function (app) {
  const io = socket(app.listen(config.SOCKET_PORT), {
    cors: {
      accept: '/*',
    },
  });
  io.on('connection', function (client) {
    var id = client.id;
    // console.log('Socket client is connected to server', client);
    console.log('Socket client is connected to server');

    // client.emit('hi', 'hi from server');
    // client.broadcast.emit('hi', 'this is broadcast..');

    client.on('new-user', function (username) {
      users.push({
        id: id,
        name: username,
      });
      client.emit('users', users);
      client.broadcast.emit('users', users);
    });

    // client.on('hello', function (data) {
    //   console.log('at hello event', data);
    // });

    client.on('new-message', function (data) {
      console.log('new messag e>>', data);
      client.emit('reply-message-own', data);
      client.broadcast.to(data.receiverId).emit('reply-message', data);
    });

    client.on('disconnect', function () {
      users.forEach(function (user, index) {
        if (user.id === id) {
          users.splice(index, 1);
        }
      });
      client.broadcast.emit('users', users);
    });
  });
};
