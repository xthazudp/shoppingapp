const router = require("express").Router();
// import routing level middleware
const AuthRouter = require('./controllers/auth.controller');
// const UserRouter = require('./controllers/user.controller');

const UserRouter = require('./modules/users/user.route');
const ProductRouter = require('./modules/products/product.route');
const CategoryRouter = require('./modules/categories/categories.route');
// const CartRouter = require('./modules/cart/cart.route');
const BraintreeRouter = require('./modules/braintree/braintree.route');
const orderRouter = require('./modules/orders/order.route');

// import application level middleware
const authenticate = require("./middlewares/authenticate");
const authorize = require("./middlewares/authorize");

router.use('/auth', AuthRouter);
router.use('/user', authenticate, UserRouter);
router.use('/product', ProductRouter);
router.use('/category', CategoryRouter);
// router.use('/cart', CartRouter);
router.use('/braintree', BraintreeRouter);
router.use('/order', orderRouter);

module.exports = router;
