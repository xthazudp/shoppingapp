const express = require('express');
const router = express.Router();

const authenticate = require('./../../middlewares/authenticate');
const { userOrderHistory } = require('../users/user.controller');

const {
  create,
  listOrders,
  getStatusValues,
  findById,
  updateOrderStatus,
} = require('./order.controller');
const { decreaseQty } = require('../products/product.controller');
const isAdmin = require('../../middlewares/isAdmin');
const authorize = require('../../middlewares/authorize');

router.post('/create/:id', authenticate, userOrderHistory, decreaseQty, create);

router.get('/list/:id', authenticate, authorize, listOrders);

router.get('/status-values/:id', authenticate, authorize, getStatusValues);

router.put('/:orderId/status/:id', authenticate, authorize, updateOrderStatus);
router.get('/:id', findById);

// router.param('orderId', orderById);

module.exports = router;
