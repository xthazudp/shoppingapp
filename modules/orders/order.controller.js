const { Order, CartItem } = require('./order.model');
const path = require('path');
// const nodemailer = require('nodemailer');
// function orderById(req, res, next) {
//   // console.log('req.param', req.param.id);
//   Order.findOne({ _id: req.params.id })
//     .populate('products.product', 'name price')
//     .exec((err, order) => {
//       if (err || !order) {
//         return next(err);
//       }
//       console.log('orders=>>', order);
//       req.order = order;
//       next();
//     });
// }

// const sender = nodemailer.createTransport({
//   service: 'Gmail',
//   auth: {
//     user: 'xthazudp2@gmail.com',
//     pass: 'account@google1940s',
//   },
// });

// function prepareEmail(data) {
//   console.log('data=>', data);
//   return {
//     from: 'noreply@shoppingApp.com', // sender address
//     to: 'xthazudp2@gmail.com', //comma seperated list of receivers
//     subject: 'A New Order is received', // Subject line
//     text: 'Forgot Password?', // plain text body
//     html: `
//     <p>Customer username : ${data.user}</p>
//     <p>Total products : ${data.length}</p>
//     <p>Total cost : ${data.amount}</p>
//     <p>Login to dashboard to the order in detail.</p>
// `,
//   };
// }

function findById(req, res, next) {
  // console.log('orderid', req.params.orderId);
  Order.findOne({ _id: req.params.orderId })
    .populate('products.product', 'name price')
    .exec(function (err, order) {
      if (err) {
        return next(err);
      }
      if (!order) {
        return next({
          msg: 'Order Not Found',
          status: 404,
        });
      }
      // console.log('order status', order);
      // req.body = order;
      // res.json(order);
      req.body = order;
      next();
    });
}

function create(req, res) {
  console.log('create order=>', req);
  req.body.order.user = req.user._id;
  const order = new Order(req.body.order);
  order.save(function (error, data) {
    if (error) {
      return next(err);
    }
    console.log('order', order);
    console.log('username', req.user.username);
    // const emailData1 = {
    //   user: req.user.username,
    //   length: order.products.length,
    //   amount: order.amount,
    // };
    // const emailBody1 = prepareEmail(emailData1);

    //   const emailData2 = {
    //     // to: req.user.email,
    //     to: 'xthazudp2@gmail.com',
    //     from: 'noreply@shoppingApp.com',
    //     subject: `You order is in process`,
    //     html: `
    //     <h1>Hey ${req.user.name}, Thank you for shopping with us.</h1>
    //     <h2>Total products: ${order.products.length}</h2>
    //     <h2>Transaction ID: ${order.transaction_id}</h2>
    //     <h2>Order status: ${order.status}</h2>
    //     <h2>Product details:</h2>
    //     <hr />
    //     ${order.products
    //       .map((p) => {
    //         return `<div>
    //                 <h3>Product Name: ${p.name}</h3>
    //                 <h3>Product Price: ${p.price}</h3>
    //                 <h3>Product Quantity: ${p.count}</h3>
    //         </div>`;
    //       })
    //       .join('--------------------')}
    //     <h2>Total order cost: ${order.amount}<h2>
    //     <p>Thank your for shopping with us.</p>
    // `,
    //   };

    // sender.sendMail(emailData2, function (err, done) {
    //   if (err) {
    //     return next(err);
    //   }
    //   next();
    // });
    // sender.sendMail(emailBody1, function (err, done) {
    //   if (err) {
    //     return next(err);
    //   }
    //   // res.json(done);
    //   next();
    // });
    res.json(data);
  });
}

function listOrders(req, res) {
  Order.find()
    .populate('user', '_id name address username createdAt')
    .sort('-createdAt')
    .exec((err, orders) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(error),
        });
      }
      res.json(orders);
    });
}

function getStatusValues(req, res) {
  res.json(Order.schema.path('status').enumValues);
}

function updateOrderStatus(req, res, next) {
  // console.log('req body', req);
  // const id = req.body._id;
  const id = req.params.orderId;

  Order.findByIdAndUpdate(
    id,
    { $set: { status: req.body.status } },
    (err, order) => {
      console.log('order', order);
      if (err) {
        return next(err);
      }
      if (!order) {
        return next({
          msg: 'order not found',
          status: 404,
        });
      }

      // console.log('status', req.body.status);
      res.send(order);
    }
  );
}

// function updateOrderStatus(req, res, next) {
//   Order.findById(req.params.orderId, function (err, order) {
//     console.log('req pramas', req.params.orderId);
//     if (err) {
//       return next(err);
//     }
//     if (!order) {
//       console.log('order=>', order);
//       return next({
//         msg: 'order not found',
//         status: 404,
//       });
//     }
//     console.log('result=>', order);

//     res.json(order);
//   });
// }

module.exports = {
  create,
  listOrders,
  getStatusValues,
  // orderById,
  findById,
  updateOrderStatus,
};
