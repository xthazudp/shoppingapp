const router = require("express").Router();
const CategoryController = require("./categories.controller");

const authenticate = require("../../middlewares/authenticate");

router
  .route("/")
  .get(authenticate, CategoryController.find)
  .post(authenticate, CategoryController.insert);

router
  .route("/:id")
  .get(authenticate, CategoryController.findById)
  .put(authenticate, CategoryController.update)
  .delete(authenticate, CategoryController.remove);
module.exports = router;
