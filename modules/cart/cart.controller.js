const cartModel = require('./cart.model');
const CartModel = require('./cart.model');

function addToCart(req, res) {
  CartModel.findOne({ user: req.user._id }).exec(function (err, cart) {
    if (err) {
      return next(err);
    }
    if (cart) {
      const product = req.body.cartItems.product;
      const item = cart.cartItems.find(function (cart) {
        cart.product = product;
      });

      if (item) {
        // if cart already exist
        CartModel.findOneAndUpdate(
          { user: req.user._id, 'cartItems.product': product },
          {
            $set: {
              cartItems: {
                ...req.body.cartItems,
                quantity: item.quantity + req.body.cartItems.quantity,
              },
            },
          }
        ).exec(function (err, cartOne) {
          if (err) {
            return next(err);
          }
          res.json(cartOne);
        });
      } else {
        // if cart already exist
        CartModel.findOneAndUpdate(
          { user: req.user._id },
          { $push: { cartItems: req.body.cartItems } }
        ).exec(function (err, cartOne) {
          if (err) {
            return next(err);
          }
          res.json(cartOne);
        });
      }
    } else {
      // if cart dont exist
      const cart = new CartModel({
        user: req.user._id,
        cartItems: [req.body.cartItems],
      });

      cart
        .save()
        .then(function (saved) {
          res.json(saved);
        })
        .catch(function (err) {
          next(err);
        });
    }
  });
}

function remove(req, res, next) {
  CartModel.findById(req.params.id, function (err, cart) {
    if (err) {
      return next(err);
    }
    if (!cart) {
      return next({
        msg: 'cart Not Found',
        status: 404,
      });
    }
    cart.remove(function (err, removed) {
      if (err) {
        return next(err);
      }
      res.json(removed);
    });
  });
}

function find(req, res, next) {
  var condition = {};

  CartModel.find(condition).exec(function (err, cart) {
    if (err) {
      return next(err);
    }
    res.json(cart);
  });
}
module.exports = {
  addToCart,
  find,
  remove,
};
