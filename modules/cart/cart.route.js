const router = require("express").Router();
const CartController = require("./cart.controller");

const authenticate = require("../../middlewares/authenticate");

router
  .route("/addtocart")
  .get(authenticate, CartController.find)
  .post(authenticate, CartController.addToCart);

router.route("/:id").delete(authenticate, CartController.remove);

module.exports = router;
