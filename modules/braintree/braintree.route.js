const express = require('express');
const router = express.Router();

const authenticate = require('./../../middlewares/authenticate');
const brainTreeController = require('./braintree.controller');

router.get('/getToken/:id', authenticate, brainTreeController.generateToken);
router.post('/payment/:id', authenticate, brainTreeController.processPayment);

module.exports = router;
