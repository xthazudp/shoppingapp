const router = require('express').Router();
const Uploader = require('./../../middlewares/uploader');
const UserController = require('./user.controller');

router.route('/').get(UserController.find);

router.route('/search').get(UserController.search);

router
  .route('/:id')
  .get(UserController.findById)
  .put(Uploader.single('image'), UserController.update)
  .delete(UserController.remove);

module.exports = router;
