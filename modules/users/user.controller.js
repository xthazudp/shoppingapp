const UserModel = require('./../../models/user.model');
const MAP_USER_REQ = require('./../../helpers/map_user_request');
const fs = require('fs');
const path = require('path');

function find(req, res, next) {
  var condition = {};
  UserModel.find(condition)
    .sort({
      _id: -1,
    })
    // .limit(2)
    // .skip(1)
    .exec(function (err, users) {
      if (err) {
        return next(err);
      }
      res.json(users);
    });
}

function findById(req, res, next) {
  const id = req.params.id;
  UserModel.findById(id, { username: 1 }, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({
        msg: 'User Not Found',
        status: 404,
      });
    }
    res.json(user);
  });
}

function update(req, res, next) {
  if (req.fileTypeError) {
    return next({
      msg: 'Invalid File Format',
      status: 400,
    });
  }
  const data = req.body;
  if (req.file) {
    data.image = req.file.filename;
  }
  // UserModel.findByIdAndUpdate
  UserModel.findById(req.params.id, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({
        msg: 'User Not Found',
        status: 404,
      });
    }
    var oldImage = user.image;
    // user exists now update
    // user is mongoose object
    var mappedUpdatedUser = MAP_USER_REQ(user, data);
    mappedUpdatedUser.updated_by = req.user.name;
    mappedUpdatedUser.save(function (err, updated) {
      if (err) {
        return next(err);
      }
      if (req.file) {
        fs.unlink(
          path.join(process.cwd(), 'uploads/images/' + oldImage),
          function (err, done) {
            if (err) {
              console.log('error in removing ', err);
            } else {
              console.log('success in removing >>', done);
            }
          }
        );
      }
      res.json(updated);
    });
  });
}

function remove(req, res, next) {
  //    UserModel.findByIdAndRemove
  UserModel.findById(req.params.id, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({
        msg: 'User Not Found',
        status: 404,
      });
    }
    user.remove(function (err, removed) {
      if (err) {
        return next(err);
      }
      res.json(removed);
    });
  });
}

function userOrderHistory(req, res, next) {
  let history = [];
  req.body.order.products.forEach((item) => {
    console.log('item=>', item);
    history.push({
      _id: item._id,
      name: item.name,
      description: item.description,
      category: item.category,
      quantity: item.count,
      // createdAt: req.body.order.createdAt,
      transaction_id: req.body.order.transaction_id,
      amount: req.body.order.amount,
    });
  });

  UserModel.findOneAndUpdate(
    { _id: req.params.id },
    { $push: { history: history } },
    { new: true },
    (err, data) => {
      if (err) {
        return next(err);
      }
      next();
    }
  );
}

function search(req, res, next) {
  res.send('from search of user');
}

module.exports = {
  // object shorthand
  find,
  findById,
  update,
  remove,
  userOrderHistory,
  search,
};
