const ProductModel = require('./product.model');
const fs = require('fs');
const path = require('path');

function map_product_req(product, productData) {
  if (productData.name) {
    product.name = productData.name;
  }
  if (productData.description) {
    product.description = productData.description;
  }
  if (productData.category) {
    product.category = productData.category;
  }
  if (productData.color) {
    product.color = productData.color;
  }
  if (productData.brand) {
    product.brand = productData.brand;
  }
  if (productData.price) {
    product.price = productData.price;
  }
  if (productData.quantity) {
    product.quantity = productData.quantity;
  }
  if (productData.images) {
    product.images = productData.images;
  }
  if (productData.status) {
    product.status = productData.status;
  }
  if (productData.tags)
    product.tags =
      typeof productData.tags === 'string'
        ? productData.tags.split(',')
        : productData.tags;
  if (productData.vendor) {
    product.vendor = productData.vendor;
  }
  // TODO handle boolean
  if (productData.warrentyStatus) {
    product.warrentyStatus = productData.warrentyStatus;
  }
  if (productData.warrentyPeroid) {
    product.warrentyPeroid = productData.warrentyPeroid;
  }

  // note flag must be in boolen
  if (
    productData.isReturnEligible == true ||
    productData.isReturnEligible == false
  ) {
    product.isReturnEligible = productData.isReturnEligible;
  }

  if (productData.salesDate) {
    product.salesDate = productData.salesDate;
  }
  if (productData.purchasedDate) {
    product.purchasedDate = productData.purchasedDate;
  }
  if (productData.manuDate) {
    product.manuDate = productData.manuDate;
  }
  if (productData.expiryDate) {
    product.expiryDate = productData.expiryDate;
  }
  if (!product.discount) {
    product.discount = {};
  }
  // handle boolean value
  if (productData.discountedItem) {
    product.discount.discountedItem = productData.discountedItem;
  }
  if (productData.discountType) {
    product.discount.discountType = productData.discountType;
  }
  if (productData.discountValue) {
    product.discount.discountValue = productData.discountValue;
  }
}

function insert(req, res, next) {
  // parse incoming data,
  // validate data
  // prepare data
  // db operation
  // req-res cycle complete
  // console.log('req.file >>>', req.file);
  // console.log('req.files >>>', req.files);
  // console.log('req.body >>>', req.body);
  if (req.fileTypeError) {
    return next({
      msg: 'Invalid File Format',
      status: 400,
    });
  }
  const data = req.body;
  // prepare data
  data.vendor = req.user._id;

  if (req.files) {
    data.images = req.files.map(function (item, index) {
      return item.filename;
    });
  }
  const newProduct = new ProductModel({});
  // newProduct is mongoose object
  map_product_req(newProduct, data);
  newProduct
    .save()
    .then(function (saved) {
      res.json(saved);
    })
    .catch(function (err) {
      next(err);
    });
}

function find(req, res, next) {
  var condition = {};

  if (req.user.role !== 1) {
    condition.vendor = req.user._id;
  }

  const perPage = (req.query.pageSize && Number(req.query.pageSize)) || 100;
  const currentPage = (req.query.pageNumber || 1) - 1;
  const skipCount = Number(currentPage * perPage);

  ProductModel.find(condition)
    // .sort()
    .limit(perPage)
    .skip(skipCount)
    .populate('vendor', {
      username: 1,
      email: 1,
    })
    // .populate('subcategory', {
    //   cName: 1,
    // })
    .populate('reviews.user')
    .exec(function (err, products) {
      if (err) {
        return next(err);
      }
      res.json(products);
    });
}

function findById(req, res, next) {
  ProductModel.findOne({ _id: req.params.id })
    .sort('-createdAt')
    .populate('reviews.user', {
      username: 1,
    })
    .exec(function (err, product) {
      if (err) {
        return next(err);
      }
      if (!product) {
        return next({
          msg: 'Product Not Found',
          status: 404,
        });
      }
      res.json(product);
    });
}

function update(req, res, next) {
  ProductModel.findById(req.params.id, function (err, product) {
    if (err) {
      return next(err);
    }
    if (!product) {
      return next({
        msg: 'Product Not Found',
        status: 404,
      });
    }
    // product found now update
    if (req.fileTypeError) {
      return next({
        msg: 'Invalid File Format',
        status: 400,
      });
    }

    const data = req.body;
    delete data.images;
    // if (req.files) {
    //   data.images = req.files.map(function (item, index) {
    //     return item.filename;
    //   });
    // }
    // data preparation
    map_product_req(product, data);

    if (data.reviewPoint && data.reviewMessage) {
      var reviewData = {};
      if (data.reviewPoint) {
        reviewData.point = data.reviewPoint;
      }
      if (data.reviewMessage) {
        reviewData.message = data.reviewMessage;
      }
      reviewData.user = req.user._id;
      product.reviews.push(reviewData);
    }

    // if (req.files) {
    //   var oldimages = product.images.map(function (item, index) {
    //     return item;
    //   });
    // }
    // if (req.files) {
    //   product.images = req.files.map(function (item, index) {
    //     return item.filename;
    //   });
    // }

    if (req.files.length > 0) {
      const newImages = req.files.map(function (item, index) {
        return item.filename;
      });

      let existingImages = [];
      if (product.images.length) {
        existingImages = product.images;
      }

      newImages.forEach(function (img) {
        existingImages.push(img);
      });

      product.images = existingImages;
    }

    product.save(function (err, updated) {
      if (err) {
        return next(err);
      }
      // for removing existing images from server
      // if (req.files) {
      //   oldimages.map(function (item, index) {
      //     fs.unlink(
      //       path.join(process.cwd(), 'uploads/images' + item),
      //       function (err, deleted) {
      //         if (err) {
      //           return next({
      //             msg: 'Error in file deletion..',
      //             status: 404,
      //           });
      //           // console.log("err in file deletion.");
      //         } else {
      //           console.log('file deleted..');
      //         }
      //       }
      //     );
      //   });
      // }
      res.json(updated);
    });
  });
}

function remove(req, res, next) {
  ProductModel.findById(req.params.id, function (err, product) {
    if (err) {
      return next(err);
    }
    if (!product) {
      return next({
        msg: 'Product Not Found',
        status: 404,
      });
    }
    product.remove(function (err, removed) {
      if (err) {
        return next(err);
      }
      res.json(removed);
    });
  });
}

function addReview(req, res, next) {
  ProductModel.findById(req.params.productId, function (err, product) {
    if (err) {
      return next(err);
    }
    if (!product) {
      return next({
        msg: 'Product Not Found',
        status: 404,
      });
    }
    // product found now update

    const data = req.body;
    // data preparation

    if (data.reviewPoint && data.reviewMessage) {
      var reviewData = {};
      if (data.reviewPoint) {
        reviewData.point = data.reviewPoint;
      }
      if (data.reviewMessage) {
        reviewData.message = data.reviewMessage;
      }
      reviewData.user = req.user._id;

      product.reviews.push(reviewData);
    } else {
      return next({
        msg: 'missing required parameters',
        status: 400,
      });
    }

    product.save(function (err, updated) {
      if (err) {
        return next(err);
      }
      res.json(updated);
    });
  });
}

function search(req, res, next) {
  console.log('req body of search =>', req.body);
  var condition = {};

  if (req.body.category) {
    condition.category = req.body.category;
  }
  if (req.body.name) {
    condition.name = req.body.name;
  }
  if (req.body.brand) {
    condition.brand = req.body.brand;
  }
  if (req.body.minPrice) {
    condition.price = {
      $gte: req.body.minPrice,
    };
  }
  if (req.body.maxPrice) {
    condition.price = {
      $lte: req.body.maxPrice,
    };
  }
  if (req.body.minPrice && req.body.maxPrice) {
    condition.price = {
      $lte: req.body.maxPrice,
      $gte: req.body.minPrice,
    };
  }
  if (req.body.fromDate && req.body.toDate) {
    const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
    const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);
    // TODO replace created at with apprpriate date property
    condition.createdAt = {
      $gte: new Date(fromDate),
      $lte: new Date(toDate),
    };
  }
  if (req.body.tags) {
    condition.tags = {
      $in: req.body.tags.split(','),
    };
  }
  // console.log('condititon is >>', condition);
  // TODO search
  ProductModel.find(condition)
    .sort('-createdAt')
    .exec(function (err, products) {
      if (err) {
        return next(err);
      }
      res.json(products);
    });
}

function list(req, res, next) {
  var condition = {};
  let sortBy = req.query.sortBy ? req.query.sortBy : 'price';
  let order = req.query.order ? req.query.order : 'asc';
  let limit = req.query.limit ? parseInt(req.query.limit) : 4;
  ProductModel.find(condition)
    // .sort({ price: 'asc' })
    .sort({ [sortBy]: [order] })
    .limit(limit)
    .exec(function (err, products) {
      if (err) {
        return next(err);
      }
      res.json(products);
    });
}

function removeExistingImage(req, res, next) {
  const productId = req.params.productId;
  // console.log('Request body of removeExistingImage==>', req.body);

  const filesToRemove = (req.body.files || []).map(function (item, index) {
    return item.split('images/')[1];
  });

  // console.log('file to remove >>', filesToRemove);

  ProductModel.findById(productId, function (err, product) {
    if (err) {
      return next(err);
    }
    product.images.forEach(function (img, index) {
      filesToRemove.forEach(function (filesToRemove, removeIdx) {
        if (img === filesToRemove) {
          product.images.splice(index, 1);
        }
      });
    });
    product.save(function (err, updated) {
      if (err) {
        return next(err);
      }
      res.json(updated);
    });
  });
}

function decreaseQty(req, res, next) {
  let bulkOps = req.body.order.products.map((item) => {
    return {
      updateOne: {
        filter: { _id: item._id },
        update: { $inc: { quantity: -item.count, sold: +item.count } },
      },
    };
  });

  ProductModel.bulkWrite(bulkOps, {}, (error, products) => {
    if (error) {
      return res.status(400).json({
        error: 'Could not update product',
      });
    }
    next();
  });
}

module.exports = {
  // object shorthand
  insert,
  find,
  findById,
  update,
  remove,
  addReview,
  search,
  list,
  removeExistingImage,
  decreaseQty,
};
