const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReviewSchema = new Schema(
  {
    point: {
      type: Number,
      min: 1,
      max: 5,
    },
    message: {
      type: String,
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'user',
    },
  },
  {
    timestamps: true,
  }
);

const ProductSchema = new Schema(
  {
    name: String,
    description: String,
    category: {
      type: String,
      required: true,
    },
    subcategory: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
    },
    color: String,
    brand: String,
    price: Number,
    quantity: Number,
    sold: {
      type: Number,
      default: 0,
    },
    images: [String], // array of string
    reviews: [ReviewSchema], // array of objects
    status: {
      type: String,
      enum: ['out of stock', 'booked', 'available'],
      default: 'available',
    },
    tags: [String],
    vendor: {
      type: Schema.Types.ObjectId,
      ref: 'user',
    },
    discount: {
      discountedItem: Boolean,
      discountType: {
        type: String,
        enum: ['quantity', 'percentage', 'value'],
      },
      discountValue: String,
    },
    warrentyStatus: Boolean,
    warrentyPeroid: String,
    isReturnEligible: Boolean,
    salesDate: Date,
    purchasedDate: Date,
    manuDate: Date,
    expiryDate: Date,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('product', ProductSchema);
