const fs = require('fs');


function myWrite(fileName, content, abcd) {
    fs.writeFile('./files/' + fileName, content, function (err, done) {
        if (err) {
            abcd(err);
        }
        else {
            abcd(null, done);
        }
    })
}

function myRead(filename) {
    return new Promise(function (resolve, reject) {
        fs.readFile('./files/' + filename, 'UTF-8', function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        })
    })
}

module.exports = {
    a: myWrite,
    b: myRead
}
